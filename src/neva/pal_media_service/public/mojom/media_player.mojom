// Copyright 2019-2020 LG Electronics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

module pal_media.mojom;

import "media/mojo/mojom/media_types.mojom";
import "media/mojo/mojom/neva/media_track_info.mojom";
import "mojo/public/mojom/base/time.mojom";
import "ui/gfx/geometry/mojom/geometry.mojom";
import "url/mojom/url.mojom";

enum CustomMessageStatus {
  kSuccess,
  kCanceled,
  kError,
};

enum Preload {
  kPreloadNone,
  kPreloadMetaData,
  kPreloadAuto,
};

enum SuspendReason {
  kBackground,
  kSuspendedByPolicy,
};

enum MediaEventType {
  kMediaEventNone,
  kMediaEventUpdateUMSMediaInfo,
  kMediaEventBroadcastErrorMsg,
  kMediaEventDvrErrorMsg,
  kMediaEventUpdateCameraState,
  kMediaEventPipelineStarted,
};

enum MediaPlayerType {
  kMediaPlayerTypeNone,
  kMediaPlayerTypeCamera,
  kMediaPlayerTypeUMS,
  kMediaPlayerTypeStub,
};

interface MediaPlayerProvider {
  CreateMediaPlayer(MediaPlayerType type, string app_id, MediaPlayer& request);
};

interface MediaPlayerListener {
  OnMediaPlayerPlay();
  OnMediaPlayerPause();
  OnPlaybackComplete();
  OnMediaError(int32 status);
  OnSeekComplete(mojo_base.mojom.TimeDelta time);
  OnMediaMetadataChanged(mojo_base.mojom.TimeDelta time, uint32 width, uint32 height, bool success);
  OnLoadComplete();
  OnVideoSizeChanged(uint32 width, uint32 height);
  OnCustomMessage(MediaEventType media_event_type, string detail);
  OnBufferingStateChanged(media.mojom.BufferingState buffering_state);
  OnTimeUpdate(mojo_base.mojom.TimeDelta current_timestamp, mojo_base.mojom.TimeTicks current_time_ticks);
  OnAudioTracksUpdated(array<media.mojom.MediaTrackInfo> audio_track_info);
  OnAudioFocusChanged();
  OnActiveRegionChanged(gfx.mojom.Rect rect);
  OnZoomAreaChanged(gfx.mojom.RectF zoom_area);
};

struct TimeDeltaPair {
  mojo_base.mojom.TimeDelta start;
  mojo_base.mojom.TimeDelta end;
};

interface MediaPlayer {
  Initialize(bool is_video,
             double current_time,
             string app_id,
             string url,
             string mime,
             string referrer,
             string user_agent,
             string cookies,
             string media_option,
             string custom_option);
  Start();
  Pause();
  Seek(mojo_base.mojom.TimeDelta time);
  SetVolume(double volume);
  SetPoster(url.mojom.Url poster);
  SetRate(double rate);
  SetPreload(Preload preload);
  [Sync] IsPreloadable(string content_media_option) => (bool result);
  [Sync] HasVideo() => (bool result);
  [Sync] HasAudio() => (bool result);
  SelectTrack(media.mojom.MediaTrackType type, string id);
  SwitchToAutoLayout();
  SetDisplayWindow(gfx.mojom.Rect out_rect,
                   gfx.mojom.Rect in_rect,
                   bool full_screen,
                   bool forced);
  [Sync] UsesIntrinsicSize() => (bool result);
  [Sync] MediaId() => (string result);
  Suspend(SuspendReason reason);
  Resume();
  [Sync] IsRecoverableOnResume() => (bool result);
  [Sync] HasAudioFocus() => (bool result);
  SetAudioFocus(bool focus);
  [Sync] HasVisibility() => (bool result);
  SetVisibility(bool visible);
  [Sync] RequireMediaResource() => (bool result);
  SetDisableAudio(bool disable);
  SetMediaLayerId(string media_layer_id);
  [Sync] GetBufferedTimeRanges() => (array<TimeDeltaPair> result);
  [Sync] Send(string message) => (bool result);

  Subscribe() => (associated MediaPlayerListener& listener);
};
